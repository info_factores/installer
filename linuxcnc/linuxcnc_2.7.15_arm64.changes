Format: 1.8
Date: Thu, 02 Jan 2020 11:59:23 -0700
Source: linuxcnc
Binary: linuxcnc-doc-en linuxcnc-doc-es linuxcnc-doc-fr linuxcnc-uspace linuxcnc-uspace-dbgsym linuxcnc-uspace-dev
Architecture: source all arm64
Version: 1:2.7.15
Distribution: unstable
Urgency: medium
Maintainer: Sebastian Kuzminsky <seb@highlab.com>
Changed-By: Sebastian Kuzminsky <seb@highlab.com>
Description:
 linuxcnc-doc-en - motion controller for CNC machines and robots (English documentat
 linuxcnc-doc-es - motion controller for CNC machines and robots (Spanish documentat
 linuxcnc-doc-fr - motion controller for CNC machines and robots (French documentati
 linuxcnc-uspace - motion controller for CNC machines and robots
 linuxcnc-uspace-dev - motion controller for CNC machines and robots (development files)
Changes:
 linuxcnc (1:2.7.15) unstable; urgency=medium
 .
   * gaxis: use the theme that was made for it
   * gmoccapy: fix unit change behavior (G20/G21)
   * gmoccapy: fix offsetpage closing after editing one cell
   * gscreen: show linked themes
   * gscreen: fix error when selecting run-from-line startpoint
   * gscreen: fix user message feature
   * gladevcp: fix a bug with ProgressBar widget
   * gladevcp: fix mode error in MDIHistory widget
   * image-to-gcode: fix a startup crash on newer distros
 .
   * stepconf: fix simulated parport invert pin error
   * pncconf: don't refer to kernel_version when not is_kernelspace (closes #159)
   * pncconf: set PID maxerror reasonably on both imperial and metric machines
   * pncconf: fix translations
   * pncconf: fix maximise script for AXIS
   * emccalib: fix parsing ini HALFILE=file items (fixes servo tuning in Axis GUI etc)
 .
   * hostmot2: fix a tram bug mostly affecting bspi (closes #451)
   * pid: use command-deriv when supplied
   * mux_generic: fix some long-standing bugs
   * time component: add pause pin to pause timing while code is paused
   * mb2hal: add support for libmodbus 3.1.2 and newer
   * hy_gy_vfd: fix modbus byte timeout
   * hm2 encoder: fix a quadrature error reporting bug
   * hm2 sserial: fix "remote error" reporting (closes #439)
   * hm2 muxed encoder: fix deskew bug (closes #394)
   * mesa_uart component: fix an old bug with name handling
 .
   * tcl hallib: fix a bug in get_netlist
   * vismach: fix import of ASCII STLs
 .
   * sample configs: add vismach 3 axis mill example
   * sample configs: use user-friendly tool table editor
   * sample configs: fix `ok_for_mdi()` example Python function homing check
 .
   * docs: fix misc typos and broken links
   * docs: [TRAJ]LINEAR_UNITS must be either mm or inch
   * docs: pwmgen has all pins now, no params
   * docs: improve lowpass manpage
   * docs: improve vismach example
   * docs: improve pyvcp container info
   * docs: add a pyvcp example
   * docs: fix incorrect pin names in hostmot2 manpage
   * docs: improve O-word examples & docs
   * docs: fix & improve G28, G30, and G92
   * docs: improve G38 & probe result params
   * docs: improve restart description in M0/M1 docs
   * docs: improve M7, M8, and M9
   * docs: add a link to the web forum to About LinuxCNC
   * docs: fix command for launching pyvcp example
   * docs: add component usage information to halcomp docs
   * docs: add arc tolerance ini settings
   * docs: improve backlash & screw compensation docs
   * docs: improve python UI library docs
   * docs: fix #<_coord_system> values
   * docs: fix indentation in remap docs example
   * docs: fix broken link in comments of comp311.ngc example program
   * docs: add missing info about motion.motion-type to Core Componens
   * docs: clarify o-word subroutine effects
   * docs: update links to point at new Wheezy ISO
   * docs: fix a broken link in French docs
   * docs: fix German translation (fixes #597)
   * docs: fix homing docs about HOME_FINAL_VEL
 .
   * Interp: fix MDI calls after sub updated (fixes #455)
   * Interp: fix motion after Abort (fixes #579)
 .
   * TP: fix fallback to parabolic blending when tangent blend fails (fixes #477)
   * TP: fix accel violation with G96 and arc blending
   * TP: fix exact-stop when falling back to alternative blend methods
   * TP: fix accel violations with near-tangent segments (fixes #546)
   * TP: apply minimum displacement checks consistently (fixes #550)
 .
   * RTAPI: allow rtapi to compile with kernel 4.14+
 .
   * make better use of autoconfigured `grep` executable in scripts
   * scripts/linuxcnc: run better in Docker
   * scripts/linuxcnc: fix cleanup log messages
 .
   * simulate_probe: rebrand EMC -> LinuxCNC in error messages
 .
   * clean up and modernize out use of yapps (for halcompile)
   * better build & packaging support for Debian 10 "Buster"
   * packaging: don't depend on python-gnome2
   * remove unused program halgui
   * fix compile error in C99 mode
   * build system: require `intltool-extract` executable
   * build system: support Ubuntu 16.04 and 18.04, and LinuxMint 18.* and 19.*
   * tests: add an Interp test of G33.1
   * tests: add an Interp test for issue #455
   * travis: use ccache
Checksums-Sha1:
 28ac96ed7eb85e203e88d4b505570c3505bd1c99 1537 linuxcnc_2.7.15.dsc
 5699230c6f0b527d2a5fa81514c66d61031f410e 20868760 linuxcnc_2.7.15.tar.xz
 e4eefda092bb58ce0ebe9428312c98c5f5f91454 12215500 linuxcnc-doc-en_2.7.15_all.deb
 011902f6f1dd9e4768fcd1708f0cae5c73767605 11361440 linuxcnc-doc-es_2.7.15_all.deb
 9fd2f78282f9bbb36e5a7e346587411a88476c45 6260260 linuxcnc-doc-fr_2.7.15_all.deb
 fa2eb751825da28ac829ad85fa446b7b131994cc 10379776 linuxcnc-uspace-dbgsym_2.7.15_arm64.deb
 8f4acaf74cb811a7a8940ac36068a61cc68c79fb 621872 linuxcnc-uspace-dev_2.7.15_arm64.deb
 eb3ec604c742e31a27673591ba51c2e395115714 3682536 linuxcnc-uspace_2.7.15_arm64.deb
 41d669b5e83ffd286062cf8f35e5d682aa0ac1ba 21298 linuxcnc_2.7.15_arm64.buildinfo
Checksums-Sha256:
 3b0994ce03b9fca244ad26d2ec9b872a259d681025a7da388fe3584446ce9298 1537 linuxcnc_2.7.15.dsc
 5cb9958b1bac647c0643f63817fe8853b9104a5d8a007bee053ead6cd02eee89 20868760 linuxcnc_2.7.15.tar.xz
 bfcc452737c5aa9e5ce4b3171e4625fa0c6b273432549ed625e40c44a9e01437 12215500 linuxcnc-doc-en_2.7.15_all.deb
 7ce35b7146d29158c09faaacc5d1dc6887369dceb81087f4913f6b5dc334ffea 11361440 linuxcnc-doc-es_2.7.15_all.deb
 1cd96aec8d1276795a5fdc80b2acc3aa7a5b869a57d3bd10819aa72be4356f96 6260260 linuxcnc-doc-fr_2.7.15_all.deb
 3ba378de5f40de6f1db1ed7222669c6e2bf258fcdb0d465147026eef3c9105b8 10379776 linuxcnc-uspace-dbgsym_2.7.15_arm64.deb
 bb0825037e605a5abdc425399ae5b9a338c23d9cbaa3b7ff6c12557968d7fe92 621872 linuxcnc-uspace-dev_2.7.15_arm64.deb
 79e8d4e42683f670fc508312dcffcf12f25e99ca976eb8cf36701e1c8445d161 3682536 linuxcnc-uspace_2.7.15_arm64.deb
 6bed51d2595d40ecbe1095f87ff5e623be6ce70057c8bfb1bc67644a3f52e602 21298 linuxcnc_2.7.15_arm64.buildinfo
Files:
 0c753788cdf02ab91364fb3481aaea56 1537 misc extra linuxcnc_2.7.15.dsc
 3df6a30c16d22f921c473fc067ad2cf9 20868760 misc extra linuxcnc_2.7.15.tar.xz
 54fcf11418587107e564d80afaf0a173 12215500 misc extra linuxcnc-doc-en_2.7.15_all.deb
 81a84b7ebe1958e33784c9e295f2fe6e 11361440 misc extra linuxcnc-doc-es_2.7.15_all.deb
 9fcceda133e619840e46b5336e42f5dc 6260260 misc extra linuxcnc-doc-fr_2.7.15_all.deb
 2782113114ae1c27bafd1357834bd2ce 10379776 debug optional linuxcnc-uspace-dbgsym_2.7.15_arm64.deb
 596226f5ea13e2361fc3eb87c4630a9b 621872 libs extra linuxcnc-uspace-dev_2.7.15_arm64.deb
 da39fb6766babd1efe93064b7032c7c7 3682536 misc extra linuxcnc-uspace_2.7.15_arm64.deb
 e0e07dff8c93f5ab86692f53ed2158a3 21298 misc extra linuxcnc_2.7.15_arm64.buildinfo
